Component({
    properties: {
        question: {
            type: String,
            value: 'question'
        },
        // 这里定义了innerText属性，属性值可以在组件使用时指定
        choices: {
            type: Array,
            value: [],
        },
        choosed: {
            
        }
    },
    data: {
        // 这里是一些组件内部数据
        radio: 1
    },
    methods: {
        // 这里是一个自定义方法
        onChange(event) {
            this.setData({
                radio: event.detail,
            });
            this.triggerEvent('GetChoice', this.data.radio)
        },
        onClick(event) {
            const { name } = event.currentTarget.dataset;
            this.setData({
                radio: name,
            }); 
            this.triggerEvent('GetChoice', this.data.radio)
        },



    }
})