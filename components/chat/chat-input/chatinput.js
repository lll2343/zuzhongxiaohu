Component({
    properties: {
        // 这里定义了innerText属性，属性值可以在组件使用时指定
    },
    data: {
        // 这里是一些组件内部数据
        iconsize: "36px",
        inputvalue: "",
        placeholdervalue: '想和TA说点什么呢？'
    },
    methods: {
        // 这里是一个自定义方法
        sendMsg: function () {
            // 父子组件传值
            this.triggerEvent("sendMsg", this.data.inputvalue);
            this.setData({
                inputvalue: ''
            })
        },

        chooseImg: function () {
            this.triggerEvent("chooseImg");
        }
    }
})
