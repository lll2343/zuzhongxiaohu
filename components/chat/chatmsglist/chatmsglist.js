Component({
    properties: {
        // 这里定义了innerText属性，属性值可以在组件使用时指定
        msg: {
            type:Object,
            value: {
                id: 0,
                isright: false,
                showSendTime: true,
                type: 'text',
                sendTime: new Date(),
                avatarimg: '/static/image/chat/extra.png',
                mgscontent: '有什么问题快像我提问吧！',
                voicelen: 5,
                imgsrc: "/static/5a6e0fcf44ec50f7.png"
            }
        }
    },
    data: {
        voiceimgpath: '/static/image/voice-white.png',
        voiceimgpathleft: '/static/image/voice-black.png'
    },
    methods: {
        // 长按删除单条消息
        // delMsg: function () {
        //     console.log("长按删除")
        //     wx.showModal({
        //         title: '提示',
        //         content: '确定删除此消息吗？',
        //         success: function (res) {
        //             if (res.confirm) {
        //                 console.log('用户点击了确认')
        //             } else if (res.cancel) {
        //                 console.log('用户点击取消')
        //             }
        //         }
        //     })
        // },

        // 点击查看大图预览
        seeBigImg: function (e) {
            console.log('大图预览');
            var src = e.currentTarget.dataset.imgurl;
            console.log(src)
            wx.previewImage({
                // current: src, // 当前显示图片的http链接
                urls: [src]
            })
        },
    }
})