const articleList = [
    {
        imgurl: "https://s3.bmp.ovh/imgs/2022/06/22/40f70617e56a1553.jpg",
        title: "为什么好好的人突然就中风了",
        describe: "",
        createTime: "2022-01-04 15:34",
        starCount: 0,
        scanCount: 0,
        id: 0
    },
    {
        imgurl: "https://s3.bmp.ovh/imgs/2022/06/22/8d4e22a37dc0866f.png",
        title: "谈谈卒中后如何康复训练",
        describe: "",
        createTime: "2022-01-04 15:34",
        starCount: 1,
        scanCount: 6,
        id: 1,
    },
    {
        imgurl: "https://s3.bmp.ovh/imgs/2022/06/22/85481f38e814d916.jpg",
        title: "脑卒中家庭急救小贴士",
        describe: "",
        createTime: "2022-01-04 15:34",
        starCount: 0,
        scanCount: 4,
        id: 2
    },
    {
        imgurl: "https://img1.imgtp.com/2022/06/22/pQOhewej.jpg",
        title: "牢记7个“中风征兆”，关键时候能救命",
        describe: "",
        createTime: "2022-01-04 15:34",
        starCount: 0,
        scanCount: 2,
        id: 3,
    },
];

const vedioList = [
    {
        imgurl: "https://i.postimg.cc/SK8h1Fwz/up.png",
        describe: "上肢训练",
        time: "228000",
        movie: "https://zuzhongxiaohu-1799204-1310977803.ap-shanghai.run.tcloudbase.com/up.mp4",
        id: 0,
    },
    {
        imgurl: "https://i.postimg.cc/DyBy1hTv/down.png",
        describe: "下肢关节活动",
        time: "168000",
        movie: "https://zuzhongxiaohu-1799204-1310977803.ap-shanghai.run.tcloudbase.com/down.mp4",
        id: 1,
    },
    {
        imgurl: "https://i.postimg.cc/5Nqp3P7N/tan.png",
        describe: "弹力带训练",
        time: "324000",
        movie: "https://zuzhongxiaohu-1799204-1310977803.ap-shanghai.run.tcloudbase.com/tan.mp4",
        id: 2,
    },
    {
        imgurl: "https://i.postimg.cc/5tHwZp3z/walk.png",
        describe: "床上和坐位康复训练",
        time: "455000",
        movie: "https://zuzhongxiaohu-1799204-1310977803.ap-shanghai.run.tcloudbase.com/walk.mp4",
        id: 3,
    }
]


const planList = [
    { title: '健康档案', finish: false, url: "/pages/patient/healthdocument/index/index" },
    { title: '每日一记', finish: false, url: "/pages/patient/measuer-day/index/index" },
    { title: '每日一测', finish: false, url: "/pages/patient/measure/index/index" },
];
const dailyPlan = [
    { title: '上肢训练', desc: "防止肌肉萎缩", finish: false, url: "/pages/tarbarpages/vedio/play/play?id=0" },
    { title: '下肢关节活动', desc: "利用下肢功率车及骑马训练器进行患侧下肢肌力训练", finish: false,url: "/pages/tarbarpages/vedio/play/play?id=1" },
    { title: '弹力带训练', desc: "利用复试墙拉力器或肩关节回旋训练器进行侧患...", finish: false,url: "/pages/tarbarpages/vedio/play/play?id=2" },
    { title: '床上和坐位康复训练', desc: "", finish: false,url: "/pages/tarbarpages/vedio/play/play?id=3" },
]

module.exports = {
    articleList, vedioList, dailyPlan, planList
}