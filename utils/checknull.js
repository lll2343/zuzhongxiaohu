/**
 * 检查是否有效的字符
 * @param {Object} s 
 * @returns 
 */
const checkValidResData=(s)=>{
    return s!= null && s!=undefined && toString(s)!="";
  }

module.exports = {
    checkValidResData
}