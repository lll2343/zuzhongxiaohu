const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [hour, minute].map(formatNumber).join(':')
  // return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}


// match the available phone number
const isPoneAvailable = (poneInput) => {
  var myreg=/^[1][3,4,5,6,7,8,9][0-9]{9}$/;
  if (!myreg.test(poneInput)) {
      return false;
  } else {
      return true;
  }
}

// match the date is the form of yyyy-MM-dd
const isyyyyMMddDate = (input) => {
  let reg = /^(\d{4})-(1[0-2]|0?[1-9])-(0?[1-9]|([1-2][0-9])|30|31)$/;
  let result = reg.test(input);
  console.log(result)
  return result
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

module.exports = {
  formatTime,
  isPoneAvailable,
  isyyyyMMddDate
}
