const host = "https://zuzhongxiaohu-1799204-1310977803.ap-shanghai.run.tcloudbase.com";
// host = "http://127.0.0.1:3000";
const get = async (url) => {
  return new Promise((resolve,reject) => {
    wx.request({
      url: host+url,
      method: "GET",
      header: {
        'Content-Type': 'application/json'
      },
      success: (res) => {
        resolve(res);
      },
      fail: (err) => {
        reject(err);
      }
    })
  })
}


/**
 * http methods of post
 * @param {string} url router path
 * @param {JSON} data senddata 
 * @returns 
 */
const post = async (url,data) => {
    return new Promise((resolve,reject) => {
        wx.request({
            url: host+url,
            method: "POST",
            data: data,
            header: {
              // 'Content-Type': 'application/json'
              'content-type': 'application/x-www-form-urlencoded', //post
            },
            success: (res) => {
              resolve(res);
            },
            fail: (err) => {
                reject(err);
            }
          });
    })
}

module.exports = {
    post,
    get,
}
