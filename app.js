// app.js
import { articleList, vedioList, planList, dailyPlan } from "./utils/articleInfo"


App({
  globalData: {
    userInfo: {
      username: "",
      imgUrl: "",
    },
    host: "127.0.0.1:3000",
    openid: "",
    session_key: "",
  },
  getCode: async () => {
    return new Promise(async (resolve, reject) => {
      wx.login({
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          console.error("error", error);
          reject(error);
        }
      })
    })
  },
  onLaunch: function () {
    // Do something initial when launch.
    wx.setStorageSync("articleList", articleList);
    wx.setStorageSync("planList", planList);
    wx.setStorageSync("dailyPlan", dailyPlan);
    wx.setStorageSync("vedioList", vedioList);
    let starList = [false, false, false, false];
    wx.setStorageSync("starList", starList);
  },
})
