# 参考资源

table组件
https://www.yiyongtong.com/archives/view-7536-1.html

## pages

1. /pages/patient/personal/index
   个人中心（页面搭建完成)
   
2. /pages/patient/measure/index
   每日一测(页面搭建完成) 添加逻辑控制，现缺少服务器交互
   
3. /pages/patient/personalinfo/index
   个人资料（页面搭建完成）缺少判断必填是否都填了
   
4. /pages/patient/measuer-day/index
   每日一记（完成）
   
5. pages/tarbarpages/home/index
   首页（）缺少轮播图部分
   
6. pages/tools/search/index/index
   搜索页面,添加空状态，缺少http
   
7. pages/patient/healthdocument/index
   健康档案（完成）
   
8. pages/patient/integral/index/index
   我的积分（完成）缺少图片替代
   
9. pages/tools/article/index/index

   详细文章页面（未开始）
   
10. pages/patient/mystar/index/index

   收藏文章页面

## router table

### 规定

使用openid做用户的唯一标识（primary key)

定义返回结果是否正确用rescode。200标识正确，400标识错误（resmsg字段标识错误原因）

sex  0 for male && 1 for female

birth   yyyy-MM-dd

### table

| router                  | function                                     | request                                                      | response                                                     |
| ----------------------- | -------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ==/user/checkreg==      | 登录界面onLoad时候用用户code到后台换取openid | {"userCode":"xxxx"}                                          | {"rescode":200 or 400 ,<br/>**openid**:"xxxxx"}<br/> rescode=200表示该用户已经在SQL里面<br/>（不是新用户，直接返回openid和个人信息）<br/>rescode=400表示该新用户 |
| ==/user/login==         | 点击授权使用进入小程序                       | {nickName:"xxx",<br/>avatarUrl:"xx",<br/>gender:0 for male or 1 for female, <br/>openid:"xxxx"} | {"rescode":200 or 400} <br/>需要将request的信息保存到SQL中，<br/>保存成功，返回200，否则400 |
| ==/user/changeinfo==    | 个人资料页面修改个人信息                     | {openid:"xxxx",<br/>imgsrc："http:xxx.com",<br/>name:"张三",sex: 0,<br> idtype:"证件类型",<br/> identifynum:"123452",<br/>relation:"",<br/>phonenumber:"1454534",<br/>defaultUser:true} | {"rescode":200 or 400} <br/>200是修改成功，400是修改失败     |
| ==/user/info==          | 个人资料页面加载时，获取系统记录的以往内容   | {"openid":"xxxx"}                                            | {rescode:200,<br/>imgsrc:"http:xxx.com",<br/>name:"张三",sex: 0,<br/> idtype:"证件类型", <br/>identifynum:"123452",<br/>relation":"",<br/>phonenumber:"1454534",<br/>defaultUser":true} or {"rescode":400} <br/>（要是上述的一些没有，就返回null或者空串 ) |
| **数据表信息**          |                                              |                                                              |                                                              |
| ==/measure/daily==      | 每日一测的数据                               | {openid:"xxx",<br/>ansList:[‘A’,'A','B']}                    | {'rescode':200 or 400} <br/>是否提交成功                     |
| ==/measure/dailyinput== | 每日一记的数据内容提交                       | {openid: "xxxx", <br/> ansList:[ <br/>  {key: "temperature", value: "12"} <br/> {key: "pressure", value: "323"} <br/>{key: "bloodsugar", value: "12"} <br/>{key: "weight", value: "12"}<br/> {key: "smoke", value: "12"} <br/> {key: "drink", value: "12"}]<br/>} | {'rescode':200 or 400} <br/>是否提交成功                     |
| /measure/document       | 健康档案                                     | ==见 补充1==                                                 | {'rescode':200 or 400} <br/>是否提交成功                     |
|                         |                                              |                                                              |                                                              |
| **搜索**                |                                              |                                                              |                                                              |
| /search/hisandrecom     | 搜索界面获得历史搜索和推荐内容               | {openid:'xxxxx'}                                             | {rescode:200,historyList:[],recomandList:[]}<br/>若openid传过去为空，则historyList设置为[] |
| /search/byvalue         | 根据关键字进行查询                           | {openid:'xxxx',<br/>value:'关键字'}                          | {rescode:200 or 400, items: []}<br/>items是搜索结果，是一个String的List |
| /search/detail          | 获取详细的信息                               | {openid:'xxxx',<br/>value:'关键字'}                          | 12                                                           |
|                         |                                              |                                                              |                                                              |
|                         |                                              |                                                              |                                                              |
|                         |                                              |                                                              |                                                              |
|                         |                                              |                                                              |                                                              |
|                         |                                              |                                                              |                                                              |
|                         |                                              |                                                              |                                                              |

#### 补充

1. 健康档案传输的数据

```json
{
    "openid":"xxxx",
    ansList:[
        {
            type: "muti",
            id:"",
            choiceList:[
           		{id:"",chooced: 0 or 1}, // 0是没选，1是选了
                ....
           	],
            others: "" 
        },
        ....
        {
        	type:"signal",
        	id:"",
        	choice:"" // -1表示无
        },
		
    ]
}
```

![1649407121179](C:\Users\86159\AppData\Roaming\Typora\typora-user-images\1649407121179.png)



## 有关文章

### 数据库

> 一张表

字段

* id
* 枚举 视频还是文章
* 创建时间
* 一张图片的url（随意取一张）或者是视频的链接
* title
* 收藏数（这个应该是有外键，被谁点赞了，自己设计）
* 点赞数

需要的操作

| 路由            | 作用               | 请求request                               | 响应response                |
| --------------- | ------------------ | ----------------------------------------- | --------------------------- |
| /search/byvalue | 按值搜索           | {openid:'xxxx',<br/>value:'查找内容'}     | {rescode:200,searchList:[]} |
| /article/star   | 给文章或者视频收藏 | {openid:'xxxx',<br/>value:文章或视频的id} | {rescode:200}               |
| /search/mystar  | 收藏记录           | {openid:'xxx'}                            | {rescode:200,searchList:[]} |

