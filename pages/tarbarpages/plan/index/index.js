// pages/tarbarpages/plan/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    minDate: new Date().getTime(),
    maxDate: new Date(2022, 5, 30).getTime(),
    selectDate: new Date().getTime(),

    planList: null,
    dailyPlan: null,
  },
  onChange: function () {

  },
  ToPage: function (ex) {
    console.log(ex)
    wx.navigateTo({
      url: ex.target.dataset.url
    })
  },

  onShow: function (options) {
    this.setData({
      planList: wx.getStorageSync('planList'),
      dailyPlan: wx.getStorageSync('dailyPlan'),
    })
  },
})