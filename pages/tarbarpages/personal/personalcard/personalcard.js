Component({
    properties: {
        // 这里定义了innerText属性，属性值可以在组件使用时指定
        loginin: {
            type:Boolean,
            default: false,
        },
        username: {
            type: String,
            default: "用户"
        },
        userlabel: {
            type: String,
            default: "个性签名"
        },
        jifen: {
            type: Number,
            default: 0
        },
        youhuiquan: {
            type: Number,
            default: 0
        },
        imgsrc: {
            type: String,
            default: "https://img.yzcdn.cn/vant/cat.jpeg"
        },
        islight: {
            type: Boolean,
            default: false
        },
    },
    lifetimes: {
        attached: function () {
            // 在组件实例进入页面节点树时执行
            // console.log(wx.getStorageSync('avatarUrl'));
            // this.setData({
            //     imgsrc: wx.getStorageSync('avatarUrl'),
            // })
        },
    },
    data: {
        // 这里是一些组件内部数据

    },
    methods: {
        NavigateToInfoPage: function(){
            wx.navigateTo({
                url:"./../../../patient/personalinfo/index"
            })
        }
    }
})