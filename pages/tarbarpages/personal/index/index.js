// pages/patient/personal/index/index.js
import Toast from "./../../../../miniprogram/miniprogram_npm/@vant/weapp/toast/toast";
import { post } from "./../../../../api/api";

Page({
  data: {
    menuList: [
      { name: "我的资料", icon: "user-o", urlto: "/pages/patient/personalinfo/index" },
      { name: "健康档案", icon: "notes-o", urlto: "/pages/patient/healthdocument/index/index" },
      // { name: "我的订阅", icon: "comment-o", urlto: "" },
      { name: "我的计划", icon: "completed", urlto: "" },
      { name: "我的收藏", icon: "goods-collect-o", urlto: "/pages/patient/mystar/index/index" },
    ],
    info: {
      loginin: true,
      imgsrc: "",
      name: "用户",
      label: "个性标签",
      jifen: 0,
      youhuiquan: 0
    },
    openid: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: async function (options) {
    let openid = wx.getStorageSync('openid');
    if (openid == "" || openid == null || openid == undefined) {
      // unlogin,has no openid
      Toast('未登录');
      return;
    }
    this.setData({
      openid: openid
    })
    // get other user info from server
    try {
      let pres = await post('/user/info', { openid: openid });
      console.log(pres);
      if (pres.statusCode != 200)
        throw '网络错误'
      if (pres.data.rescode != 200)
        throw '服务器错误'

      let info2 = this.data.info;
      info2.imgsrc = pres.data.imgsrc,
      info2.name = pres.data.name
      console.log(info2)
      this.setData({
        info: info2
      })
    } catch (ex) {
      console.log(ex);
      Toast(ex);
    }
  },

  ExitLogin: function () {
    console.log("退出登录");
    let info2 = this.data.info;
    info2.name = '用户'
    info2.jifen = 0
    info2.youhuiquan = 0
    info2.label = '用户标签'
    info2.imgsrc = ''
    this.setData({
      info: info2
    })

    // 清除缓存
    wx.clearStorage()
    wx.navigateTo({
      url: '/pages/index/index?exit=true',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
