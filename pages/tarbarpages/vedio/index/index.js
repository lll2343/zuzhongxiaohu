// pages/tarbarpages/vedio/index/index.js
Page({
  data: {
    searchValue: "",
    showOverLay: false,
    vedioList: null
  },

  // 挑战到搜索页面
  navigateToSearchPage: function () {
    wx.navigateTo({
      url: "/pages/tools/search/index/index"
    })
  },

  onLoad: function (options) {
    this.setData({
      vedioList: wx.getStorageSync('vedioList'),
    })
  },

  // 转到详情页
  GetDetailPage: function (ex) {
    console.log('vedio navigate to', ex.currentTarget.dataset);
    let ix = ex.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/pages/tarbarpages/vedio/play/play?id="+ix,
    })
  },

  onPullDownRefresh: function () {

  },
})
