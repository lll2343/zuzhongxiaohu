// pages/tarbarpages/vedio/play/play.js
Page({
  data: {
    vediosrc: "http://www.w3school.com.cn//i/movie.mp4",
    item: "肘部支撑",
    time: 12 * 60 * 1000,
    timeData: {},
    paused: true,
  },
  onChange(e) {
    this.setData({
      timeData: e.detail,
    });
  },
  // 停止或开始播放
  ControlCount: function () {
    if (this.data.paused) {
      this.start();
      var videoplay = wx.createVideoContext('video')
      videoplay.play()
    } else {
      this.pause();
      var videoplay = wx.createVideoContext('video')
      videoplay.pause()
    }
    this.setData({
      paused: !this.data.paused
    })
  },

  start() {
    const countDown = this.selectComponent('#countdown');
    countDown.start();
  },

  pause() {
    const countDown = this.selectComponent('#countdown');
    countDown.pause();
  },

  reset() {
    const countDown = this.selectComponent('#countdown');
    countDown.reset();
  },

  onLoad: function (options) {
    // console.log(options,'on load');
    let vedioItem = wx.getStorageSync("vedioList")[Number(options.id)];
    console.log(vedioItem);
    this.setData({
      time: vedioItem.time,
      item: vedioItem.describe,
      vediosrc: vedioItem.movie
    })
  },
})