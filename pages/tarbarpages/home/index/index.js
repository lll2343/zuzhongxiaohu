// pages/tarbarpages/home/index.js
Page({
  data: {
    searchValue: "",
    background: ['demo-text-1', 'demo-text-2', 'demo-text-3'],
    // recomandList: [
    //   'https://s3.bmp.ovh/imgs/2022/06/22/40f70617e56a1553.jpg',
    //   'https://s3.bmp.ovh/imgs/2022/06/22/8d4e22a37dc0866f.png',
    //   'https://s3.bmp.ovh/imgs/2022/06/22/85481f38e814d916.jpg',
    //   'https://img1.imgtp.com/2022/06/22/pQOhewej.jpg'
    // ],
    articleList: null,
    showOverLay: false
  },

  onSearch: function () {
    // 搜索
    console.log(this.data.searchValue);
  },

  getMore: function () {
    console.log("更多内容");
    wx.navigateTo({
      url: '/pages/tools/recomandList/index/index'
    })
  },

  HandleClickContent: function (ex) {
    console.log(ex);
    this.setData({
      showOverLay: true
    })
    // await this.getSerachItems(ex.target.dataset.value);
    let url = '/pages/tools/article/index/index?id='
      + ex.currentTarget.dataset.id
      + '&starCount=' + ex.target.dataset.starcount
      + '&scanCount=' + ex.target.dataset.scancount;
    wx.navigateTo({
      url: url,
    })

    this.setData({
      showOverLay: false
    })
  },

  navigateToSearchPage: function () {
    wx.navigateTo({
      url: "/pages/tools/search/index/index"
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      articleList: wx.getStorageSync("articleList")
    })
  },
})
