// pages/tools/recomandList/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "推荐内容",
    articleList: null
  },

  onShow: function (options) {
    this.setData({
      articleList: wx.getStorageSync("articleList")
    });
    console.log(this.data.articleList);
  },

  // 返回首页
  backToHomePage: function () {
    console.log("back");
    wx.switchTab({
      url: '/pages/tarbarpages/home/index/index',
    })
  },

  // 转到详情页
  GetDetailPage: function (ex) {
    // console.log(ex.currentTarget.dataset)
    let url = '/pages/tools/article/index/index?id='
      + ex.currentTarget.dataset.id
      + '&starCount=' + ex.currentTarget.dataset.starcount
      + '&scanCount=' + ex.currentTarget.dataset.scancount;
    wx.navigateTo({
      url: url,
    })
  }
})