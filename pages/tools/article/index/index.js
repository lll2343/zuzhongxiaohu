// pages/tools/article/index/index.js
Page({
  data: {
    curarticle: 3,
    statr: false,
    starCount: 2,
    scanCount: 5,
    hasStar: false
  },

  onLoad: function (options) {
    console.log(options, 'onload');
    this.setData({
      curarticle: options.id,
      starCount: options.starCount,
      scanCount: options.scanCount,
    });
    let storageList = wx.getStorageSync("articleList");
    storageList[this.data.curarticle].scanCount += 1;
    wx.setStorageSync("articleList", storageList);
  },

  GetStar: function () {
    let newStar = this.data.hasStar ? Number(this.data.starCount) - 1 : Number(this.data.starCount) + 1;
    this.setData({
      starCount: newStar,
      hasStar: !(this.data.hasStar)
    })
    
    let storageList = wx.getStorageSync("articleList");
    storageList[this.data.curarticle].starCount = newStar;
    wx.setStorageSync("articleList", storageList);
    console.log(wx.getStorageSync("articleList"));

    storageList = wx.getStorageSync("starList");
    storageList[this.data.curarticle] = this.data.hasStar;
    wx.setStorageSync("starList",storageList);
  }
})