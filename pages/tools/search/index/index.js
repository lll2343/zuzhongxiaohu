// pages/tools/search/index.js
import { post } from "./../../../../api/api"
import Toast from "./../../../../miniprogram/miniprogram_npm/@vant/weapp/toast/toast"
import { articleList } from "./../../../../utils/articleInfo"
Page({
  data: {
    openid: null,
    searchValue: "", // 搜索框输入
    showOverLay: false,
    found: 1,
    historyList: [],
    articleList: null,
    searchResult: [],
  },

  // 根据条件搜索，
  getSerachItems: async function (value) {
    try {
      let res = await post('/search/byvalue', {
        openid: this.data.openid,
        value: value
      });

      if (res.statusCode != 200)
        throw '网络错误'
      if (res.data.rescode != 200)
        throw '服务器错误'

      if (res.data.items == null) {
        this.setData({
          found: 0
        })
      }
      else {
        this.setData({
          found: 2,
          searchResult: res.data.items
        })
      }
    } catch (error) {
      console.log(error);
      Toast(error);
    }
  },

  // 搜索
  onSearch: async function () {
    let that = this;
    this.setData({
      showOverLay: true
    })
    // 搜索
    // console.log(this.data.searchValue);

    if (this.data.searchValue == null || this.data.searchValue == "") {
      Toast('搜索内容为空');
      this.setData({
        found: 1,
        showOverLay: false
      })
    } else {
      setTimeout(() => {
        that.setData({
          found: 2,
          showOverLay: false
        })
      }, 800)
    }
    // await this.getSerachItems(this.data.searchValue);
    // this.setData({
    //   showOverLay: false
    // })
  },

  // 跳转到详情页面
  HandleSerach: async function (ex) {
    // console.log(ex)
    console.log(ex.target.dataset.value)
    this.setData({
      showOverLay: true
    })
    // await this.getSerachItems(ex.target.dataset.value);
    let url = '/pages/tools/article/index/index?id='
      + ex.currentTarget.dataset.id
      + '&starCount=' + ex.target.dataset.starcount
      + '&scanCount=' + ex.target.dataset.scancount;
    wx.navigateTo({
      url: url,
    })

    this.setData({
      showOverLay: false
    })
  },

  onLoad: async function (options) {
    let openid = wx.getStorageSync('openid');
    this.setData({
      articleList: articleList
    })
    console.log('openid', openid)
    if (openid == "" || openid == null || openid == undefined) {
      // unlogin,has no openid
      Toast('未登录');
      // this.setData({
      //   historyList: []
      // })
      return;
    }
    // 获取历史搜索记录和推荐内容
    // try {
    //   let res = await post('/search/hisandrecom', {
    //     openid: openid
    //   })
    //   if (res.statusCode != 200)
    //     throw '网络错误'
    //   if (res.data.rescode != 200)
    //     throw '服务器错误'

    //   this.setData({
    //     historyList: res.data.historyList,
    //     recomandList: res.data.recomandList
    //   })
    // } catch (error) {
    //   console.log(error);
    //   Toast(error);
    // }
  },

  // // 搜索获取详情
  // HandleGetDetail: async function (ex) {
  //   console.log(ex.target.dataset.value);
  //   // 搜索并跳转至详情页
  //   this.setData({
  //     showOverLay: true
  //   })
  //   try {
  //     // let res = post('/search/detail', {
  //     //   openid: this.data.openid,
  //     //   value: ex.target.dataset.value
  //     // })
  //     // if (res.statusCode != 200)
  //     //   throw '网络错误'
  //     // if (res.data.rescode != 200)
  //     //   throw '服务器错误'

  //     wx.navigateTo({
  //       url: '/pages/tools/article/index/index',
  //       /* .... other variables */
  //     })
  //   } catch (error) {
  //     console.log(error)
  //     Toast(error)
  //   }

  //   this.setData({
  //     showOverLay: false
  //   })
  // },

  // 返回首页
  backToHomePage: function () {
    console.log("back")
    wx.switchTab({
      url: '/pages/tarbarpages/home/index/index',
    })
  },
})