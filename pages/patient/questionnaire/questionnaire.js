// pages/patient/questionnaire/questionnaire.js
import Notify from './../../../miniprogram/miniprogram_npm/@vant/weapp/notify/notify'

Page({
  data: {
    questionNaires: [
      {
        question: '1.你是否头晕',
        choices: [
          { context: 'A.是', choice: 'A' },
          { context: 'B.不是', choice: 'B' },
        ]
      }, 
      {
        question: '2.心烦',
        choices: [
          { context: 'A.轻微', choice: 'A' },
          { context: 'B.稍微', choice: 'B' },
          { context: 'C.较重', choice: 'C' },
        ]
      },
      {
        question: '3.心烦',
        choices: [
          { context: 'A.轻微', choice: 'A' },
          { context: 'B.稍微', choice: 'B' },
          { context: 'C.较重', choice: 'C' },
        ]
      },
      {
        question: '4.心烦',
        choices: [
          { context: 'A.轻微', choice: 'A' },
          { context: 'B.稍微', choice: 'B' },
          { context: 'C.较重', choice: 'C' },
        ]
      },
      {
        question: '5.心烦',
        choices: [
          { context: 'A.轻微', choice: 'A' },
          { context: 'B.稍微', choice: 'B' },
          { context: 'C.较重', choice: 'C' },
          { context: 'D.很重', choice: 'D' },
        ]
      },
    ],
    answer: null,
    process: 0,
    currentQuestion: 0,
    totalQuestion: 0,
  },

  ChangeProcess: function (e) {
    let inc = e.target.dataset.inc;
    let newProcess = 0, newCurrentQuestion = this.data.currentQuestion;
    let step = 100 / this.data.totalQuestion;
    if (inc == "true") {
      newProcess = this.data.process + step;
      newCurrentQuestion += 1;
      if (newProcess > 100) {
        Notify('无更多题目');
        return;
      }
    } else {
      newProcess = this.data.process - step;
      newCurrentQuestion -= 1;
      if (newProcess < 0) {
        Notify('不能更少了');
        return;
      }
    }
    this.setData({
      process: newProcess,
      currentQuestion: newCurrentQuestion
    })
  },

  GetChoice: function (e) {
    console.log('父组件', e.detail);
    console.log(this.data.currentQuestion);
    let index = this.data.currentQuestion
    let arr = this.data.answer;
    arr[index] = e.detail;
    this.setData({
      answer:arr
    })
    console.log(this.data.answer);
  },

  QuitAnswer: function (e) {
    console.log('放弃')
  },

  FinishAnswer: function(e){
    if(this.CheckAllAnswering()){
      console.log('完成')
    }
    else {
      Notify('有未做完的题目');
    }
  },

  CheckAllAnswering: function(){
    let flag = true;
    this.data.answer.forEach(element => {
      if(element == -1){
        flag = false
      }
    });
    return flag;
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let len = this.data.questionNaires.length;
    console.log(len);
    let arr = new Array(len).fill(-1);

    console.log(arr);
    this.setData({
      answer: arr,
      totalQuestion: len
    })
    console.log(this.data.answer)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})