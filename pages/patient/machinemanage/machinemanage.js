// pages/patient/machinemanage/machinemanage.js
Page({
  data: {
    tableHeader: [
      {
        prop: 'type',
        width: 180,
        label: '药物',
      },
      {
        prop: 'eat_times',
        width: 170,
        label: '服用次数'
      },
      {
        prop: 'eat_mount',
        width: 200,
        label: '服用量'
      },
      {
        prop: 'eat_method',
        width: 200,
        label: '服用方法'
      }
    ],
    stripe: true,
    border: true,
    outBorder: true,
    row: [
      {
          "id": 1,
          "type": 'a',
          "eat_times": "04-01",
          "eat_mount": '09:30:00',
          "eat_method": '18:30:00',
      }, {
          "id": 2,
          "type": 'b',
          "eat_times": "04-02",
          "eat_mount": '10:30:00',
          "eat_method": '18:30:00',
      }, {
          "id": 29,
          "type": 'c',
          "eat_times": "04-03",
          "eat_mount": '09:30:00',
          "eat_method": '18:30:00',
      }, {
          "id": 318,
          "type": 'd',
          "eat_times": "04-04",
          "eat_mount": '',
          "eat_method": '',

      }, {
          "id": 319,
          "type": 'e',
          "eat_times": "04-05",
          "eat_mount": '09:30:00',
          "eat_method": '18:30:00',
      }
    ],
    msg: '暂无数据'
  },

  /** 
   * 点击表格一行
   */
   onRowClick: function(e) {
    console.log('e: ', e.detail)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.initData()
  },

  initData: function(){
    wx.setNavigationBarTitle({
      title: '用药管理'
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})