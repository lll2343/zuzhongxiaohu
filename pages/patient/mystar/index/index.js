// pages/patient/mystar/index/index.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: "我的收藏",
    empty: true,
    starBoolList: [],
    starList: []
  },

  backToHomePage: function () {
    console.log("back")
    wx.switchTab({
      url: '/pages/tarbarpages/personal/index/index',
    })
  },

  onShow: function () {
    this.setData({
      starList: wx.getStorageSync("articleList"),
      starBoolList: wx.getStorageSync("starList"),
    });
    console.log(this.data.starBoolList);
    for (let i = 0; i < this.data.starBoolList.length; i++) {
      if (this.data.starBoolList[i] == true) {
        this.setData({
          empty: false
        });
        break;
      }
    }
  },
})