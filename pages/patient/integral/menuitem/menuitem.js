Component({
    properties: {
      menu: {
          type: Object,
          default: {}
      }
    },
    data: {
      
    },
    lifetimes: {
      ready() {
        
      },
    },
    methods: {
      // 处理传值事件
      HandelSendMsg: function () {
        this.triggerEvent("getChoice", { id: this.data.id, choice: this.data.radio });
      },

      HandleTaskClick: function(){
        console.log("去完成");
      },
      
      onChange(event) {
        this.setData({
          radio: event.detail,
        });
        this.HandelSendMsg();
      },
      onClick(event) {
        const { name } = event.currentTarget.dataset;
        this.setData({
          radio: name,
        });
        this.HandelSendMsg();
      },
  
    }
  })