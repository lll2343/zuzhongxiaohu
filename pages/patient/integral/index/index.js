// pages/patient/integral/index/index.js
import Toast from "./../../../../miniprogram/miniprogram_npm/@vant/weapp/toast/toast"
import {post} from "./../../../../api/api";
Page({
  data: {
    info: {
      imgsrc: "https://thirdwx.qlogo.cn/mmopen/vi_32/hSXmd9IZuY8J…cGl9fM2pBlxS4XTPYJZA7o6aVA4bekOibB5buvT4Wo7oA/132",
      name: "刘亮",
      label: "个性标签",
      jifen: 0,
      youhuiquan: 0
    },
    menuList: [
      { title: "记录身体数据", desc: "成功记录身体数据可获得30分", urlto: "" },
      { title: "学习任意课程", desc: "学习任意课程获得成长值，已得到0/500分", urlto: "" },
      { title: "打卡个性定制计划", desc: "打卡个性化定制计划任务获得，已得到0/200分", urlto: "" },
      { title: "完成每日一测", desc: "成功完成每日一测可获得30分", urlto: "" },
    ]
  },
  onLoad: function (params) {
    
  },
  onShow: async function (options) {
    let openid = wx.getStorageSync('openid');
    if (openid == "" || openid == null || openid == undefined) {
      // unlogin,has no openid
      Toast('未登录');
      return;
    }
    this.setData({
      openid: openid
    })
    // get other user info from server
    try {
      let pres = await post('/user/info', { openid: openid });
      console.log(pres);
      if (pres.statusCode != 200)
        throw '网络错误'
      if (pres.data.rescode != 200)
        throw '服务器错误'

      let info2 = this.data.info;
      info2.imgsrc = pres.data.imgsrc,
      info2.name = pres.data.name
      console.log(info2)
      this.setData({
        info: info2
      })
    } catch (ex) {
      console.log(ex);
      Toast(ex);
    }
  },

})
