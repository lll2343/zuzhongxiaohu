// pages/patient/healthdocument/index/index.js
import Toast from "./../../../../miniprogram/miniprogram_npm/@vant/weapp/toast/toast"
import { post } from "./../../../../api/api"

Page({
  data: {
    showOverLay: false,
    mutipletimu: [{
      id: 0,
      desc: "您现在是否患有以下疾病?",
      type: "可多选",
      choicelist: [{ id: 0, desc: "高血压" }, { id: 1, desc: "糖尿病" },
      { id: 2, desc: "高血脂" }, { id: 3, desc: "痛风" },
      { id: 4, desc: "心脏病" }, { id: 5, desc: "中风" },
      { id: 6, desc: "颈椎病" }, { id: 7, desc: "腰椎病" },
      { id: 8, desc: "肾病" }, { id: 9, desc: "其他" }],
      other: "其他什么疾病？"
    },
    {
      id: 1,
      desc: "您最近有以下症状吗?（近三个月内）",
      type: "可多选",
      choicelist: [{ id: 0, desc: "头晕/头痛" }, { id: 1, desc: "口腔溃疡/牙痛" },
      { id: 2, desc: "食欲不正/无精打采" }, { id: 3, desc: "腰/肩酸痛" },
      { id: 4, desc: "肠胃不适/便秘" }, { id: 5, desc: "尿频尿急" },
      { id: 6, desc: "其他" }],
      other: "其他什么症状？"
    },
    {
      id: 2,
      desc: "您有下列家族病史吗？（父母、祖父母、兄弟、姐妹）",
      type: "可多选",
      choicelist: [{ id: 0, desc: "高血压" }, { id: 1, desc: "糖尿病" },
      { id: 2, desc: "高血脂" }, { id: 3, desc: "心脏病" },
      { id: 4, desc: "癌症" }, { id: 5, desc: "其他" }],
      other: "其他什么家族病史？"
    },
    {
      id: 3,
      desc: "您日常饮食习惯?",
      type: "可多选",
      choicelist: [{ id: 0, desc: "油炸" }, { id: 1, desc: "辛辣" },
      { id: 2, desc: "冷冻" }, { id: 3, desc: "火锅" },
      { id: 4, desc: "烧烤" }, { id: 5, desc: "生食" },
      { id: 6, desc: "甜食" }, { id: 7, desc: "清淡" }]
    },
    {
      id: 4,
      desc: "对于常见的健康问题，您比较在意的是什么?（最多三项）",
      type: "可多选",
      choicelist: [{ id: 0, desc: "高血压" }, { id: 1, desc: "糖尿病" },
      { id: 2, desc: "高血脂" }, { id: 3, desc: "痛风" },
      { id: 4, desc: "心脏病" }, { id: 5, desc: "颈肩腰背痛" },
      { id: 6, desc: "脑血管病" }, { id: 7, desc: "胃肠炎" },
      { id: 8, desc: "支气管炎" }, { id: 9, desc: "其他" }],
      other: "其他在意的健康问题？"
    },
    ],
    mutipleAns: null,

    signalTimu: [{
      id: 0,
      isnecessary: true,
      desc: "您每天喝水量？（每杯250ml）",
      type: "单选",
      chooselist: [
        { name: 1, desc: "3杯以下" },
        { name: 2, desc: "3~5杯" },
        { name: 3, desc: "5~8杯" },
        { name: 4, desc: "8杯以上" }
      ]
    },
    {
      id: 1,
      isnecessary: true,
      desc: "您的作息规律吗？",
      type: "单选",
      chooselist: [
        { name: 1, desc: "早睡早起" },
        { name: 2, desc: "晚睡早起" },
        { name: 3, desc: "晚睡晚起" },
        { name: 4, desc: "看心情" }
      ]
    },
    {
      id: 2,
      isnecessary: true,
      desc: "您每天睡眠时间？",
      type: "单选",
      chooselist: [
        { name: 1, desc: "小于7小时" },
        { name: 2, desc: "7~8小时" },
        { name: 3, desc: "大于8小时" }
      ]
    },
    {
      id: 3,
      isnecessary: true,
      desc: "您每周运动频率？（30分钟）",
      type: "单选",
      chooselist: [
        { name: 1, desc: "不运动" },
        { name: 2, desc: "1~2次" },
        { name: 3, desc: "3~5次" },
        { name: 4, desc: "6~7次" }
      ]
    }],
    signalAns: null,

    openid: null
  },

  onLoad: function () {
    let newlist = new Array(this.data.mutipletimu.length);
    let newlist2 = new Array(this.data.signalTimu.length);
    for (let i = 0; i < this.data.mutipletimu.length; i++) {
      let ischoosed = new Array(this.data.mutipletimu[i].choicelist.length);
      for (let j = 0; j < this.data.mutipletimu[i].choicelist.length; j++) {
        ischoosed[j] = {
          id: j,
          choosed: 0,
        }
      }

      newlist[i] = {
        id: i,
        type: "muti",
        choiceList: ischoosed,
        others: ''
      }
    }
    // console.log(newlist);
    for (let i = 0; i < this.data.signalTimu.length; i++) {
      newlist2[i] = { type: "signal", id: i, choice: -1 }
    }
    // console.log(newlist2)
    let openid = wx.getStorageSync('openid');
    if (openid == "" || openid == null || openid == undefined) {
      // unlogin,has no openid
      Toast('未登录')
      return;
    }
    this.setData({
      mutipleAns: newlist,
      signalAns: newlist2,
      openid: openid
    })
  },

  handleSignalChoice: function (ex) {
    let { id, choice } = ex.detail;;
    let tmpAnsList = this.data.signalAns;
    tmpAnsList[id] = {
      type: "signal",
      id: id,
      choice: Number(choice)
    };
    this.setData({
      signalAns: tmpAnsList
    })
    console.log(this.data.signalAns);
  },

  judgeIfChoosed(ix, result) {
    for (let i = 0; i < result.length; i++) {
      if (ix == result[i]) {
        return 1;
      }
    }
    return 0;
  },

  handleMutiChoice: function (ex) {
    let { id, result, others } = ex.detail;
    let tmpAnsList = this.data.mutipleAns;
    let choiceListLen = this.data.mutipletimu[id].choicelist.length
    let ischoosed = new Array(choiceListLen);
    console.log(result);
    for (let i = 0; i < choiceListLen; i++) {
      ischoosed[i] = {
        id: i,
        choosed: this.judgeIfChoosed(i, result),
      }
    }
    tmpAnsList[id] = {
      id: id,
      type: "muti",
      choiceList: ischoosed,
      others: others
    };
    this.setData({
      mutipleAns: tmpAnsList
    })
    console.log(this.data.mutipleAns);
  },

  handleRecord: function () {
    console.log('记录监测记录');
  },

  PostSendData: async function (sendData) {
    let openid = this.data.openid;
    if (openid == "" || openid == null || openid == undefined) {
      return 'empty openid';
    }
    try {
      // wx.request({
      //   url: "http://10.132.48.104:8080"+"/measure/document",
      //   method: "POST",
      //   data: sendData,
      //   header: {
      //     'Content-Type': 'application/json'
      //     // 'content-type': 'application/x-www-form-urlencoded', //post
      //   },
      //   success: (res) => {
      //     console.log(res)
      //   },
      //   fail: (err) => {
      //     console.log(err)
      //   }
      // });

      let pres = await post("/measure/document", sendData);
      if (pres.statusCode != 200)
        throw 'network error';
      if (pres.data.rescode != 200) {
        if (pres.data.msg)
          throw `error ${pres.data.msg}`;
        else
          throw `error`;
      }
      return '提交成功';
    } catch (error) {
      return error
    }
  },

  handleSave: async function () {
    console.log('保存')
    let sendData = {
      openid: this.data.openid,
      ansList: this.data.mutipleAns.concat(this.data.signalAns),
    }
    console.log(sendData);
    // console.log(sendData.ansList[0]);
    this.setData({
      showOverLay: true
    })
    let pres = await this.PostSendData(sendData);
    Toast(pres);
    if (pres == '提交成功') {
      let storageList = wx.getStorageSync("planList");
      storageList[0].finish = true;
      wx.setStorageSync("planList", storageList);
      setTimeout(() => {
        wx.switchTab({
          url: '/pages/tarbarpages/home/index/index',
        })
      }, 500)
    }
    this.setData({
      showOverLay: false
    });
  }
})
