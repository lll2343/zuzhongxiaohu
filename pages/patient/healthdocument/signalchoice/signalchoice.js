Component({
    properties: {
      // 这里定义了innerText属性
      timu: {
        type: Object,
      }
    },
    data: {
      // 这里是一些组件内部数据
      radio: '1', // 选项
      id: 0, // 题目标识
    },
    lifetimes: {
      ready() {
        // 提取出题目的第一个字符作为标识（要是多于10个那么就不好处理了）
        this.setData({
          id: this.properties.timu.id,
        })
      },
    },
    methods: {
      // 处理传值事件
      HandelSendMsg: function () {
        this.triggerEvent("getChoice", { id: this.data.id, choice: this.data.radio });
      },
      onChange(event) {
        this.setData({
          radio: event.detail,
        });
        this.HandelSendMsg();
      },
      onClick(event) {
        const { name } = event.currentTarget.dataset;
        this.setData({
          radio: name,
        });
        this.HandelSendMsg();
      },
  
    }
  })