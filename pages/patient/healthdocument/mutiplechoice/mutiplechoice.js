Component({
    properties: {
      // 这里定义了innerText属性
      timu: {
        type: Object,
      }
    },
    data: {
      // 这里是一些组件内部数据
      result: [],
      others: ""
    },
    lifetimes: {
      ready() {
        // 获取标识符
        this.setData({
          id: this.properties.timu.id,
        })
      },
    },
    methods: {
      // 处理传值事件
      HandelSendMsg: function () {
        this.triggerEvent("getChoice", { id: this.data.id, result: this.data.result,others:this.data.others });
      },
      onChange(event) {
        this.setData({
          result: event.detail,
        });
        this.HandelSendMsg();
      },
      changInput: function(){
        this.HandelSendMsg();
      }
    }
  })