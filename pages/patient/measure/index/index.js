// pages/patient/measure/index/index.js
import Toast from "./../../../../miniprogram/miniprogram_npm/@vant/weapp/toast/toast";
import { post } from "./../../../../api/api"


Page({
  data: {
    openid: null,

    showOverLay: false, // 遮罩层
    title: {
      p: "脑卒中风险自评问卷",
      desc: "脑卒中俗称中风,包括缺血性脑卒中(脑梗死)和出血性脑卒中两种。脑卒中具有发病率、致残率、复发率和死亡率高的特点,寒冷季节发病率更高。及早发现卒中风险、及时干预、救治,对于疾病预防具有重要意义。"
    },
    timuList: [
      {
        isnecessary: true,
        desc: "1、您患有高血压吗？(血压≥140/90mmHg)",
        type: "单选",
        chooselist: [
          { name: "A", desc: "A、是" },
          { name: "B", desc: "B、否" }
        ]
      },
      {
        isnecessary: true,
        desc: "2、您患有糖尿病吗？",
        type: "单选",
        chooselist: [
          { name: "A", desc: "A、是" },
          { name: "B", desc: "B、否" }
        ]
      },
      {
        isnecessary: true,
        desc: "3、您患有心房颤动疾病吗？（心跳不规则）",
        type: "单选",
        chooselist: [
          { name: "A", desc: "A、是" },
          { name: "B", desc: "B、否" }
        ]
      },
      {
        isnecessary: true,
        desc: "4、您患有同型半胱氨酸血症吗？（Hcy值＞10umol/L）",
        type: "单选",
        chooselist: [
          { name: "A", desc: "A、是" },
          { name: "B", desc: "B、否" }
        ]
      },
      {
        isnecessary: true,
        desc: "5、您平时吸烟吗？",
        type: "单选",
        chooselist: [
          { name: "A", desc: "A、是" },
          { name: "B", desc: "B、否" }
        ]
      },
      {
        isnecessary: true,
        desc: "6、您的体重超重吗？（BMI=体重÷身高2.体重单位：kg,身高单位：m。BMI≥24为超重）",
        type: "单选",
        chooselist: [
          { name: "A", desc: "A、是" },
          { name: "B", desc: "B、否" }
        ]
      },
      {
        isnecessary: true,
        desc: "7、您平时缺乏运动吗？",
        type: "单选",
        chooselist: [
          { name: "A", desc: "A、是" },
          { name: "B", desc: "B、否" }
        ]
      },
      {
        isnecessary: true,
        desc: "8、您有卒中家族史吗？",
        type: "单选",
        chooselist: [
          { name: "A", desc: "A、是" },
          { name: "B", desc: "B、否" }
        ]
      },
    ],
    ansList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let tmpAnsList = new Array(this.data.timuList.length);
    this.setData({
      ansList: tmpAnsList,
      openid: wx.getStorageSync('openid')
    })
  },

  // 处理子组件的传值事件
  handleGetChoice: function (ex) {
    let { id, choice } = ex.detail;;
    let tmpAnsList = this.data.ansList;
    tmpAnsList[id - 1] = choice;
    this.setData({
      ansList: tmpAnsList
    })
  },

  // 判断是否全部都填写了
  judgeFullFilled: function () {
    if (this.data.openid == null || this.data.openid == undefined || this.data.openid == "") {
      return { flag: false, ix: 0 };
    }
    let ix = 0;
    for (; ix < this.data.ansList.length; ix++) {
      let element = this.data.ansList[ix];
      if (element == undefined || element == null || element == "") {
        break;
      }
    };
    if (ix == this.data.ansList.length) {
      return { flag: true, ix: ix };
    } else {
      return { flag: false, ix: ix };
    }
  },

  PostDataOfMeasure: async function () {
    console.log(this.data.openid)
    console.log(this.data.ansList)
    try {
      let pres = await post("/measure/daily", {
        openid: this.data.openid,
        ansList: this.data.ansList,
      })
      if (pres.statusCode != 200)
        throw '错误';
      if (pres.data.rescode != 200)
        throw '错误';
      return '提交成功';
    } catch (ex) {
      console.log(ex);
      return ex;
    }
  },

  // 提交
  handleSubmit: async function () {
    let { flag, ix } = this.judgeFullFilled();
    if (flag) {
      // axios send data
      Toast.success(`正在上传`);
      this.setData({
        showOverLay: true
      })
      let res = await this.PostDataOfMeasure();
      Toast(res);
      if (res == '提交成功') {
        let storageList = wx.getStorageSync("planList");
        storageList[2].finish = true;
        wx.setStorageSync("planList", storageList);
        setTimeout(() => {
          wx.switchTab({
            url: '/pages/tarbarpages/home/index/index',
          })
        }, 500)
      }
      this.setData({
        showOverLay: false
      })
    } else {
      if (ix == 0)
        Toast.fail('未作答');
      else
        Toast.fail(`${ix + 1}未作答`);
    }
  },
})