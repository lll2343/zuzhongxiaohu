import * as echarts from './../ec-canvas/echarts';

function initChart(canvas, width, height, dpr) {
  const chart = echarts.init(canvas, null, {
    width: width,
    height: height,
    devicePixelRatio: dpr // new
  });
  canvas.setChart(chart);

  var option = {
    xAxis: {
      type: 'category',
      boundaryGap: false,
      show: false
    },
    yAxis: {
      x: 'center',
      type: 'value',
      splitLine: {
        lineStyle: {
          type: 'dashed'
        }
      }
      // show: false
    },
    series: [{
      name: 'A',
      type: 'line',
      smooth: true,
      data: [18, 36, 65, 30, 78, 40, 33]
    }]
  };

  chart.setOption(option);
  return chart;
}


Component({
    properties: {
      timu: {
        type: Object,
        default: null
      }
    },
    data: {
      isedit: false,
      value: "",
      key:null,
      ec: {
        onInit: initChart
      }
    },
    lifetimes: {
      ready() {
       this.setData({
        key: this.properties.timu.name,
       })
      },
    },
    methods: {
      clickEdit: function(){
        console.log("edit");
        this.setData({
          isedit: true
        })
      },
      finishEdit: function(){
        this.setData({
          isedit: false
        })
        this.HandelSendMsg();
      },
      // 处理传值事件
      HandelSendMsg: function () {
        this.triggerEvent("getInput", {  
          value: this.data.value,
          key:this.data.key,
          id:this.properties.timu.id 
        });
      },
    }
  })