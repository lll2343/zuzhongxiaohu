var utils = require("../../utils/util.js")

/**
 * 右边的消息
 * @param {Number} id 
 * @param {String} avatarimg 
 * @param {String} mgscontent 
 * @returns 
 */
const creatTextMsg = (id,avatarimg,mgscontent,isright=true)=>{
    return {
        isright: isright,
        showSendTime: false,
        type: 'text',
        sendTime: utils.formatTime(new Date()),
        avatarimg: avatarimg,
        mgscontent: mgscontent
    };
}

const createImgMsg = (id,avatarimg,imgsrc,isright=true)=>{
    return {
        isright: isright,
        showSendTime: false,
        type: 'img',
        sendTime: utils.formatTime(new Date()),
        avatarimg: avatarimg,
        voicelen: 5,
        imgsrc: imgsrc,
    };
}

module.exports = {
    creatTextMsg,
    createImgMsg
}
